# All Weather

This is a calculator for investing using the all weather strategy. This
strategy was popularized by Ray Dalio when he wanted to create a resilient and
balance portfolio.

This calculator takes the current portfolio breakdown and the new investment
amount. It then calculates the required split of the investment to balance the
portfolio.
