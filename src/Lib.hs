{-#LANGUAGE ScopedTypeVariables#-}

module Lib
    ( run
    ) where

import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe

type Portfolio = Map String Float

percentages :: Portfolio
percentages = Map.fromList
  [ ("commodities", 7.5)
  , ("gold", 7.5)
  , ("mtt", 15)
  , ("stocks", 30)
  , ("ltt", 40)
  ]

run :: IO ()
run = do
  putStrLn "Long Term Treasuries: "
  ltt :: Float <- readLn
  putStrLn "Stocks: "
  stocks :: Float <- readLn
  putStrLn "Mid Term Treasuries: "
  mtt :: Float <- readLn
  putStrLn "Gold: "
  gold :: Float <- readLn
  putStrLn "Commodities: "
  commodities :: Float <- readLn
  putStrLn "New Investment: "
  investment :: Float <- readLn
  let currentPortfolio =
        Map.fromList 
        [ ("commodities", commodities)
        , ("gold", gold)
        , ("mtt", mtt)
        , ("stocks", stocks)
        , ("ltt", ltt)
        ]
      idealPortfolio = calculateIdealPortfolio currentPortfolio investment 
      investmentSplit = Map.unionWith (-) idealPortfolio currentPortfolio
      prettyPrintedSplit = Map.mapWithKey prettyPrintItem investmentSplit
   in do
     putStrLn "Your investments should be: "
     executePrettyPrintMap "ltt" prettyPrintedSplit
     executePrettyPrintMap "stocks" prettyPrintedSplit
     executePrettyPrintMap "mtt" prettyPrintedSplit
     executePrettyPrintMap "gold" prettyPrintedSplit
     executePrettyPrintMap "commodities" prettyPrintedSplit

calculateIdealPortfolio :: Portfolio -> Float -> Portfolio
calculateIdealPortfolio currentPortfolio investment =
  let
    currentTotal = foldr (+) 0 currentPortfolio
    newTotal = currentTotal + investment
  in Map.map (\percent -> newTotal * (percent / 100)) percentages

prettyPrintItem :: String -> Float -> IO ()
prettyPrintItem k v = putStrLn $ k ++ ": " ++ (show v)

executePrettyPrintMap :: String -> Map String (IO ()) -> IO()
executePrettyPrintMap key map = fromJust $ Map.lookup key map
